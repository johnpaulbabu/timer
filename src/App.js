import React, {useState, useEffect} from 'react'
import { Button } from '@material-ui/core'
import './components/styles.css'

const App = () => {

    const [count, setCount] = useState(0)
    const [buttonactive, setButtonactive] = useState(false)
    const [buttonstatus, setButtonstatus] = useState("Start")

    const Toggle = ()=>{
        setButtonactive(!buttonactive)
    }
    const reset =()=>setCount(0)

    useEffect(() => {
      let interval = null
      if(buttonactive) {
            interval = setInterval(() => {
            setCount(prevCount => prevCount + 1)
          }, 1000)
       }
       else if(!buttonactive) {
          clearInterval(interval)
        }
       return()=>clearInterval(interval)
      }, [buttonactive])
  
    useEffect(()=>{
      if(buttonactive) {
        setButtonstatus(prevButton=>"Pause")
      }
      
      else if(!buttonactive && count===0){
        setButtonstatus(prevButton=>"Start")
      }
      else if(!buttonactive && count !== 0) {
        setButtonstatus(prevButton=>"Resume")
      }

     },[buttonactive,count])
    
   
   return(
       <div className="timer">
            <h1>{count} s</h1>
            <Button variant="outlined" color="primary" onClick={Toggle}>{buttonstatus}</Button>
            <Button variant="outlined" color="secondary" onClick={reset}>Reset</Button>
       </div>
  ) 
}
export default App
